<?php

namespace Lodgify;

use Cake\Cache\Cache;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Lodgify\Response;
use GuzzleHttp\Client as GuzzleHttpClient;
use I18n\Lib\Lang;

class Client
{
    const TIMEOUT = 20;
    const CACHE_CONFIG = 'avail';
    const ENDPOINT = 'https://api.lodgify.com';

    private $apiKey;

    private $http;

    private $language = 'es';


    public function __construct($api_key)
    {
        if (empty($api_key)) {
            throw new \Exception("No API KEY provided", 1);
        }

        $this->apiKey = $api_key;
        $this->http = new GuzzleHttpClient();
    }

    public function cache()
    {
        $args = func_get_args();
        $key = md5(serialize($args));
        $method = $args[0];
        unset($args[0]);

        if (($response = Cache::read($key, self::CACHE_CONFIG)) === false) {
            $response = call_user_func_array([$this, $method], $args);
            Cache::write($key, $response, self::CACHE_CONFIG);
        }

        return $response;
    }

    private function getRequestUri($uri)
    {
        return self::ENDPOINT . $uri;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    public function setLanguageApp()
    {
        $this->language = Lang::current('iso2');
        return $this;
    }

    public function request($method, $uri, array $query = [], $cache = false)
    {

        $options = [
            'headers' => [
                'X-ApiKey' => $this->apiKey,
                'Accept-Language' => $this->language
            ],
        ];

        if (self::TIMEOUT > 0) {
            $options['timeout'] = self::TIMEOUT;
            $options['connect_timeout'] = self::TIMEOUT;
        }

        if ($method == 'GET' && !empty($query)) {
            $options['query'] = $query;
        } elseif (in_array($method, ['POST', 'PATCH', 'PUT'])) {
            $options['json'] = $query;
            $options['headers']['Content-Type'] = 'application/json';
        }


        if ($cache) {
            $key = md5($method . $uri . serialize($query));
            $results = Cache::read($key, 'availability');
        } else {
            $results = false;
        }

        if (!$results) {
            try {
                $time = microtime(true);
                //\Cake\Log\Log::debug("Init request $method $uri");
                $response = $this->http->request($method, $this->getRequestUri($uri), $options);
                \Cake\Log\Log::debug("Lodgify request $method ".$this->getRequestUri($uri)." in " . (microtime(true) - $time));
                \Cake\Log\Log::debug($options);
                
                $lodgifyResponse = new Response($response);
                $results = $lodgifyResponse->getJson();

                if ($cache) {
                    Cache::write($key, $results, 'availability');
                }
            } catch (\Throwable $th) {
                \Cake\Log\Log::debug("Abort request $method $uri in " . (microtime(true) - $time));

                $event = new Event('Lodgify.Client.afterClientRequest', $this, []);
                EventManager::instance()->dispatch($event);

                $results = false;
            }
        }

        return $results;
    }

    public function getProperties()
    {
        $response = $this->request('GET', "/v2/properties");
        return $response['items'];
    }

    public function getProperty($id)
    {
        $response = $this->request('GET', "/v2/properties/$id");
        return $response;
    }

    public function getRooms(string $property_id)
    {
        return $this->request('GET', "/v2/properties/$property_id/rooms");
    }

    public function getQuote(string $property_id, string $room_id, $arrival, $departure, $people, $addons, $promo_code = false)
    {
        $data = [
            'arrival' => $arrival,
            'departure' => $departure,
            'roomTypes' => [
                [
                    '.Id' => $room_id,
                    '.People' => $people,
                ]
            ],
            'addOns' => $addons,
        ];

        if ($promo_code) {
            $data['promotionCode'] = $promo_code;
        }

        $query = urldecode(http_build_query($data));
        $query = str_replace('[.Id]', '.Id', $query);
        $query = str_replace('[.People]', '.People', $query);
        $response = $this->request('GET', "/v2/quote/$property_id?$query");
        return $response;
    }

    public function getAddons(string $property_id)
    {
        return $this->request('GET', "/v1/properties/$property_id/rates/addons");
    }

    public function getCountries()
    {
        return $this->request('GET', "/v1/countries");
    }

    public function getRates(string $property_id, string $room_id)
    {
        return $this->request('GET', "/v2/quote/$property_id", [
            'arrival' => '2021-06-01',
            'departure' => '2021-06-02',
            'roomTypes' => [
                'Id' => $room_id,
                'People' => 2
            ]
        ]);
    }

    public function createQuote(string $reserve_id, string $room_id, $addons, $messages = null)
    {
        $data = [
            'is_policy_active' => false,
            'room_types' => [
                [
                    'room_type_id' => $room_id,
                ]
            ],
            'add_ons' => $addons,
        ];


        return $this->request('POST', "/v1/reservation/booking/$reserve_id/quote", $data);
    }

    public function payQuote(string $reserve_id, $amount)
    {
        $data = [
            'amount' => $amount
        ];

        return $this->request('POST', "/v1/reservation/booking/$reserve_id/quote/paymentLink", $data);
    }

    public function payQuote2(string $reserve_id, $amount)
    {
        $data = [
            'amount' => $amount
        ];

        return $this->request('PUT', "/v1/reservation/booking/$reserve_id/request_payment");
    }

    public function getAvailability($start, $end, $property_id = false)
    {
        $query = [
            'start' => $start,
            'end' => $end
        ];

        $url = "/v2/availability";

        if ($property_id) {
            $query['propertyId'] = $property_id;
            $url .= '/' . $property_id;
        }

        return $this->request('GET', $url, $query, true);
    }


    public function reserve($data)
    {
        $this->setLanguageApp();
        $data['guest']['locale'] = Lang::current('locale');
        $data['payment_website_id'] = 79082;
        return $this->request('POST', "/v1/reservation/booking", $data);
    }

    public function finalizeReserve($id)
    {
        $this->setLanguageApp();
        return $this->request('PUT', "/v1/reservation/booking/$id/book");
    }

    public function getReserve($id)
    {
        $this->setLanguageApp();
        return $this->request('GET', "/v1/reservation/booking/$id");
    }

    public function cancelReserve($id)
    {
        $this->setLanguageApp();
        return $this->request('PUT', "/v1/reservation/booking/$id/decline");
    }


    public function message($id, $subject, $message)
    {
        return $this->request('POST', "/v1/reservation/booking/$id/messages", [
            'subject' => $subject,
            'message' => $message,
            'type' => 2
        ]);
    }
}
