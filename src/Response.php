<?php 

namespace Lodgify;


class Response
{
    /**
     * @var \GuzzleHttp\Psr7\Response $response
     */
    private $response;

    public function __construct(\GuzzleHttp\Psr7\Response $response)
    {
        $this->response = $response;
    }

    public function getJson()
    {
        return json_decode($this->response->getBody()->getContents(), true);
    }
}